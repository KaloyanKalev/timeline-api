module.exports = {
// Method which can be used for dynamic creation and give response with any model
  async createRecord(model, obj, res) {
    let createdObj = {};

    try {
      createdObj = await model.create(obj).fetch();
    } catch(e) {
      res.json({
        status: 500,
        message: e
      });
    }

    if(createdObj) {
      res.json({
            data: createdObj,
            status: 200,
            message: `Record successfully created.`
          });
    }
  },
};
