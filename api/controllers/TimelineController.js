/**
 * TimelineController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  // [POST method] -> create post body req args -> {username: 'string', post: 'string'}
  createPost(req,res) {
    ResponseUtil.createRecord(Timeline, req.body, res);
  },

  // [POST method] -> create comment body req args -> {username: 'string', comment: 'string', postId: 'id of the post'}
  createComment(req, res) {
    ResponseUtil.createRecord(Comment, req.body, res);
  },

  // list timeline [GET METHOD]
  async listTimeline(req,res) {
    try {
      res.json({
        data: await Timeline.find().populate('comments'),
        status: 200
      });
    } catch(err) {
      res.serverError(err);
    }
  }
};

