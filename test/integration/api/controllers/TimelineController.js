var request = require('supertest'),
    should = require('should'),
    postId = '';

describe('TimelineController', function () {

    before(function (done) {
        done(null, sails);
    });

    it('should create post', function (done) {
      request(sails.hooks.http.app)
          .post('/create_post')
          .send({username: 'Test', post: "Testing Post"})
          .expect(200)
          .end(function (err, res) {
              if (err) return done(err);
              should.exist(res.body.data);
              postId = res.body.data.id;
              done();
          });
    });

    it('should create comment', function (done) {
      request(sails.hooks.http.app)
          .post('/create_comment')
          .send({username: 'Test', comment: "Testing Comment", postId: postId})
          .expect(200)
          .end(function (err, res) {
              if (err) return done(err);
              should.exist(res.body.data);
              done();
          });
    });

    it('should receive timeline', function (done) {
      request(sails.hooks.http.app)
          .get('/list_timeline')
          .expect(200)
          .end(function (err, res) {
              if (err) return done(err);
              should.exist(res.body.data);
              done();
          });
    });

});
